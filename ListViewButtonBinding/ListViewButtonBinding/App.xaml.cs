﻿
using Xamarin.Forms;

namespace ListViewButtonBinding
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

    
    }
}
