﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace ListViewButtonBinding
{

    public class MainViewModel 
    {

        public ICommand OnClick
        {
            get
            {
                if (this.onClick == null)
                    this.onClick = new Command<ItemViewModel>(item =>
                      {
                          PageHelper.Alert("提示", $"您按下了{item.Value}");
                      });
                return this.onClick;
            }
        }
        private ICommand onClick;

        public List<ItemViewModel> Items { get; set; }
        public MainViewModel()
        {
            this.Items = new List<ItemViewModel>();
            for (int i = 1; i <= 100; i++)
                this.Items.Add(new ItemViewModel() { Value = $"我是第{i}" });
        }
    }

}
